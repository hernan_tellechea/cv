//
//  Contest.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class Contest {
    let contestName: String?
    let place: Int?
    
    init(_ dictionary: Dictionary<String, Any>) {
        self.contestName = dictionary["contestName"] as? String
        self.place = dictionary["place"] as? Int
    }
}
