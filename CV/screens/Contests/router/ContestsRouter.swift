//
//  SummaryDetailRouter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class ContestsRouter: ContestsPresenterToRouterProtocol {
    static func createModule() -> ContestsViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "ContestsViewController") as! ContestsViewController
        
        let presenter: ContestsViewToPresenterProtocol & ContestsInteractorToPresenterProtocol = ContestsPresenter()
        let interactor: ContestsPresenterToInteractorProtocol = ContestsInteractor()
        let router: ContestsPresenterToRouterProtocol = ContestsRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
