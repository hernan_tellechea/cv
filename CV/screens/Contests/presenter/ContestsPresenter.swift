//
//  SummaryDetailPresenter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class ContestsPresenter: ContestsViewToPresenterProtocol {
    var view: ContestsPresenterToViewProtocol?
    var interactor: ContestsPresenterToInteractorProtocol?
    var router: ContestsPresenterToRouterProtocol?
}

extension ContestsPresenter: ContestsInteractorToPresenterProtocol {

}
