//
//  ProjectCell.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class ContestCell: UITableViewCell {
    @IBOutlet weak var medalImageView: UIImageView!
    @IBOutlet weak var contestTitleLabel: UILabel!
}
