//
//  SummaryDetailViewController.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class ContestsViewController: UIViewController {

    var presenter: ContestsViewToPresenterProtocol?
    var contests: [Contest]?
    
    let firstPlaceMedalImage = UIImage(named: "medal1")
    let secondPlaceMedalImage = UIImage(named: "medal2")
    let thirdPlaceMedalImage = UIImage(named: "medal3")
    let medalImage = UIImage(named: "medal")

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension ContestsViewController: ContestsPresenterToViewProtocol {
    func show(contests: [Contest]) {
        self.contests = contests
    }
}

extension ContestsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contests?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContestCell", for: indexPath) as! ContestCell
        cell.contestTitleLabel.text = self.contests?[indexPath.row].contestName
        switch self.contests?[indexPath.row].place ?? 0 {
        case 1:
            cell.medalImageView.image = self.firstPlaceMedalImage
        case 2:
            cell.medalImageView.image = self.secondPlaceMedalImage
        case 3:
            cell.medalImageView.image = self.thirdPlaceMedalImage
        default:
            cell.medalImageView.image = self.medalImage
        }
        return cell
    }
}

extension ContestsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}
