//
//  SummaryDetailProtocols.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

protocol ContestsViewToPresenterProtocol: class {
    
    var view: ContestsPresenterToViewProtocol? {get set}
    var interactor: ContestsPresenterToInteractorProtocol? {get set}
    var router: ContestsPresenterToRouterProtocol? {get set}
}

protocol ContestsPresenterToViewProtocol: class {
    func show(contests:[Contest])
}

protocol ContestsPresenterToRouterProtocol: class {
    static func createModule() -> ContestsViewController
}

protocol ContestsPresenterToInteractorProtocol: class {
    var presenter:ContestsInteractorToPresenterProtocol? {get set}
}

protocol ContestsInteractorToPresenterProtocol: class {

}
