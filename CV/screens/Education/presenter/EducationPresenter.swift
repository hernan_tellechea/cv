//
//  SummaryDetailPresenter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class EducationPresenter: EducationViewToPresenterProtocol {
    var view: EducationPresenterToViewProtocol?
    var interactor: EducationPresenterToInteractorProtocol?
    var router: EducationPresenterToRouterProtocol?
}

extension EducationPresenter: EducationInteractorToPresenterProtocol {

}
