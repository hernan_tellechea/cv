//
//  SummaryDetailViewController.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class EducationViewController: UIViewController {

    @IBOutlet weak var universityLabel: UILabel!
    @IBOutlet weak var universityImageView: UIImageView!
    @IBOutlet weak var careerLabel: UILabel!
    @IBOutlet weak var graduationLabel: UILabel!
    @IBOutlet weak var gpaLabel: UILabel!
    @IBOutlet weak var thesisLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: EducationViewToPresenterProtocol?
    var education: Education?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.universityImageView.layer.cornerRadius = self.universityImageView.frame.width / 2
        self.updateUI()
    }
    
    func updateUI() {
        self.universityLabel.text = self.education?.university
        self.careerLabel.text = self.education?.career
        self.graduationLabel.text = self.education?.graduation
        if let gpa = self.education?.gpa {
            self.gpaLabel.text = "\(gpa)"
        }
        self.thesisLabel.text = self.education?.thesis
        if let imageLink = self.education?.photo, let url = URL(string: imageLink) {
            self.universityImageView.load(url: url)
        }
        self.tableView.reloadData()
    }
    
}

extension EducationViewController: EducationPresenterToViewProtocol {
    func show(education: Education) {
        self.education = education
        self.updateUI()
    }
}

extension EducationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Projects"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.education?.projects.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectCell", for: indexPath) as! ProjectCell
        cell.projectTitleLabel.text = self.education?.projects[indexPath.row]
        return cell
    }
}

extension EducationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}
