//
//  ProjectCell.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class ProjectCell: UITableViewCell {
    @IBOutlet weak var projectTitleLabel: UILabel!
}
