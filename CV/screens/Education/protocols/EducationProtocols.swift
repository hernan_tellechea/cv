//
//  SummaryDetailProtocols.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

protocol EducationViewToPresenterProtocol: class {
    
    var view: EducationPresenterToViewProtocol? {get set}
    var interactor: EducationPresenterToInteractorProtocol? {get set}
    var router: EducationPresenterToRouterProtocol? {get set}
}

protocol EducationPresenterToViewProtocol: class {
    func show(education:Education)
}

protocol EducationPresenterToRouterProtocol: class {
    static func createModule() -> EducationViewController
}

protocol EducationPresenterToInteractorProtocol: class {
    var presenter:EducationInteractorToPresenterProtocol? {get set}
}

protocol EducationInteractorToPresenterProtocol: class {

}
