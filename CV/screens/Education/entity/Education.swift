//
//  Education.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class Education {
    let career: String?
    let university: String?
    let photo: String?
    let graduation: String?
    let gpa: Double?
    var projects: [String]
    let thesis: String?
    var extracurricularActivities: [String]
    
    init(_ dictionary: Dictionary<String, Any>) {
        self.career = dictionary["career"] as? String
        self.university = dictionary["university"] as? String
        self.photo = dictionary["photo"] as? String
        self.graduation = dictionary["graduation"] as? String
        self.gpa = dictionary["gpa"] as? Double
        self.thesis = dictionary["thesis"] as? String
        if let projectsArray = dictionary["projects"] as? Array<String> {
            self.projects = projectsArray
        }
        else {
            self.projects = []
        }
        if let extracurricularActivitiesArray = dictionary["extracurricularActivities"] as? Array<String> {
            self.extracurricularActivities = extracurricularActivitiesArray
        }
        else {
            self.extracurricularActivities = []
        }
    }
}
