//
//  SummaryDetailRouter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class EducationRouter: EducationPresenterToRouterProtocol {
    static func createModule() -> EducationViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "EducationViewController") as! EducationViewController
        
        let presenter: EducationViewToPresenterProtocol & EducationInteractorToPresenterProtocol = EducationPresenter()
        let interactor: EducationPresenterToInteractorProtocol = EducationInteractor()
        let router: EducationPresenterToRouterProtocol = EducationRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
