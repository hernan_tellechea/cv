//
//  ProjectPresenter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class ProjectPresenter: ProjectViewToPresenterProtocol {
    
    var view: ProjectPresenterToViewProtocol?
    var interactor: ProjectPresenterToInteractorProtocol?
    var router: ProjectPresenterToRouterProtocol?
}

extension ProjectPresenter: ProjectInteractorToPresenterProtocol {

}
