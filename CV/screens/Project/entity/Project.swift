//
//  Project.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class Project {
    let name: String?
    let photo: String?
    let link: String?
    var details: [String]
    
    init(_ dictionary: Dictionary<String, Any>) {
        self.name = dictionary["name"] as? String
        self.photo = dictionary["photo"] as? String
        self.link = dictionary["link"] as? String
        if let detailsArray = dictionary["details"] as? Array<String> {
            self.details = detailsArray
        }
        else {
            self.details = []
        }
    }
}
