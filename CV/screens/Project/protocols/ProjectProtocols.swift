//
//  ProjectProtocols.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

protocol ProjectViewToPresenterProtocol: class{
    
    var view: ProjectPresenterToViewProtocol? {get set}
    var interactor: ProjectPresenterToInteractorProtocol? {get set}
    var router: ProjectPresenterToRouterProtocol? {get set}
}

protocol ProjectPresenterToViewProtocol: class{

}

protocol ProjectPresenterToRouterProtocol: class {
    static func createModule() -> ProjectViewController
}

protocol ProjectPresenterToInteractorProtocol: class {
    var presenter:ProjectInteractorToPresenterProtocol? {get set}
}

protocol ProjectInteractorToPresenterProtocol: class {

}
