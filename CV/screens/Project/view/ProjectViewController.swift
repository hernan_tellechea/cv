//
//  ProjectViewController.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class ProjectViewController: UIViewController {

    @IBOutlet weak var projectImageView: UIImageView!
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var openAppStoreButtonHeightConstraint: NSLayoutConstraint!
    
    var presenter: ProjectViewToPresenterProtocol?
    var project: Project?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let imageLink = project?.photo, let url = URL(string: imageLink) {
            self.projectImageView.load(url: url)
        }
        self.projectNameLabel.text = self.project?.name
        if let _ = self.project?.link{
            self.openAppStoreButtonHeightConstraint.constant = 50
        }
        else {
            self.openAppStoreButtonHeightConstraint.constant = 0
        }
    }
    
    @IBAction func didTapOpenAppStore(_ sender: Any) {
        if let link = self.project?.link, let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension ProjectViewController: ProjectPresenterToViewProtocol {

}

extension ProjectViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.project?.details.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath) as! DetailsCell
        let details = self.project?.details[indexPath.row]
        cell.detailsLabel.text = details
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Details"
    }
}

extension ProjectViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
}
