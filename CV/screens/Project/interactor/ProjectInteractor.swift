//
//  ProjectInteractor.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

class ProjectInteractor: ProjectPresenterToInteractorProtocol {
    var presenter: ProjectInteractorToPresenterProtocol?
}
