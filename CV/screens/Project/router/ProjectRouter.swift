//
//  ProjectRouter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class ProjectRouter: ProjectPresenterToRouterProtocol {
    static func createModule() -> ProjectViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "ProjectViewController") as! ProjectViewController
        
        let presenter: ProjectViewToPresenterProtocol & ProjectInteractorToPresenterProtocol = ProjectPresenter()
        let interactor: ProjectPresenterToInteractorProtocol = ProjectInteractor()
        let router: ProjectPresenterToRouterProtocol = ProjectRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
