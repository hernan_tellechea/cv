//
//  SummaryDetailInteractor.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

class SummaryDetailInteractor: SummaryPresenterToInteractorProtocol {
    
    let urlString = "https://gist.githubusercontent.com/xandarei/33b4ef147a8499dd7d0b050222d2f5ae/raw/9295cb1d68b31c3233b6c8766593d0d9bb4ff373/summary"
    var presenter: SummaryInteractorToPresenterProtocol?
    
    func fetchSummary() {
        let url = URL(string: urlString)!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                self.presenter?.summaryFetchFailed()
                return
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any>, let dic = json?["content"] as? Dictionary<String, Any> else {
                self.presenter?.summaryFetchFailed()
                return
            }
            let summary = Summary(dic)
            self.presenter?.summaryFetchedSuccess(summaryModel: summary)
        }
        task.resume()
    }
}
