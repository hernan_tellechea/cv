//
//  SummaryDetailViewController.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class SummaryDetailViewController: UIViewController {

    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var statementLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var presenter: SummaryViewToPresenterProtocol?
    var summary: Summary?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        SummaryDetailRouter.createModule(withViewController: self)
        self.startIndicatingActivity()
        self.presenter?.startFetchingSummary()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.photoImageView.layer.cornerRadius = self.photoImageView.frame.width / 2
    }
    
    
    @IBAction func didTapEducation(_ sender: Any) {
        if let education = self.summary?.education, let navigationController = self.navigationController {
            self.presenter?.showEducationController(navigationController: navigationController, withData: education)
        }
    }
    
    @IBAction func didTapContests(_ sender: Any) {
        if let contests = self.summary?.programmingContests, let navigationController = self.navigationController {
            self.presenter?.showContestsController(navigationController: navigationController, withData: contests)
        }
    }
}

extension SummaryDetailViewController: SummaryPresenterToViewProtocol {
    func show(summary: Summary) {
        DispatchQueue.main.async {
            self.summary = summary
            self.fullNameLabel.text = summary.personalData?.fullName
            self.statementLabel.text = summary.personalStatement
            if let age = summary.personalData?.age {
                self.ageLabel.text = "\(age)"
            }
            self.emailLabel.text = summary.personalData?.email
            self.mobileLabel.text = summary.personalData?.mobile
            self.addressLabel.text = summary.personalData?.address
            if let imageLink = summary.personalData?.photo, let url = URL(string: imageLink) {
                self.photoImageView.load(url: url)
            }
            self.stopIndicatingActivity()
        }
    }
    
    func showError() {
        DispatchQueue.main.async {
            self.stopIndicatingActivity()
            let alert = UIAlertController(title: "Alert", message: "There was a problem fetching the summary, please try again later.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
