//
//  SummaryDetailRouter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class SummaryDetailRouter: SummaryPresenterToRouterProtocol {
    static func createModule() -> SummaryDetailViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "SummaryDetailViewController") as! SummaryDetailViewController
        
        let presenter: SummaryViewToPresenterProtocol & SummaryInteractorToPresenterProtocol = SummaryDetailPresenter()
        let interactor: SummaryPresenterToInteractorProtocol = SummaryDetailInteractor()
        let router:SummaryPresenterToRouterProtocol = SummaryDetailRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static func createModule(withViewController view: SummaryDetailViewController)  {
        
        let presenter: SummaryViewToPresenterProtocol & SummaryInteractorToPresenterProtocol = SummaryDetailPresenter()
        let interactor: SummaryPresenterToInteractorProtocol = SummaryDetailInteractor()
        let router:SummaryPresenterToRouterProtocol = SummaryDetailRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushToEducationScreen(navigationController: UINavigationController, withData education: Education) {
        let vc = EducationRouter.createModule()
        vc.education = education
        navigationController.pushViewController(vc,animated: true)
        
    }
    
    func pushToContestsScreen(navigationController: UINavigationController , withData contests: [Contest]) {
        let vc = ContestsRouter.createModule()
        vc.contests = contests
        navigationController.pushViewController(vc,animated: true)
    }
}
