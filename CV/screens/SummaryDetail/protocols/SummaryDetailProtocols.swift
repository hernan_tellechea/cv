//
//  SummaryDetailProtocols.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

protocol SummaryViewToPresenterProtocol: class{
    
    var view: SummaryPresenterToViewProtocol? {get set}
    var interactor: SummaryPresenterToInteractorProtocol? {get set}
    var router: SummaryPresenterToRouterProtocol? {get set}
    func startFetchingSummary()
    func showEducationController(navigationController:UINavigationController, withData education: Education)
    func showContestsController(navigationController:UINavigationController, withData contests: [Contest])
}

protocol SummaryPresenterToViewProtocol: class{
    func show(summary:Summary)
    func showError()
}

protocol SummaryPresenterToRouterProtocol: class {
    static func createModule() -> SummaryDetailViewController
    func pushToEducationScreen(navigationController: UINavigationController, withData education: Education)
    func pushToContestsScreen(navigationController: UINavigationController, withData contests: [Contest])
}

protocol SummaryPresenterToInteractorProtocol: class {
    var presenter:SummaryInteractorToPresenterProtocol? {get set}
    func fetchSummary()
}

protocol SummaryInteractorToPresenterProtocol: class {
    func summaryFetchedSuccess(summaryModel:Summary)
    func summaryFetchFailed()
}
