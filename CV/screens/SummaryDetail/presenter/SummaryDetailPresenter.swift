//
//  SummaryDetailPresenter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class SummaryDetailPresenter: SummaryViewToPresenterProtocol {
    
    var view: SummaryPresenterToViewProtocol?
    var interactor: SummaryPresenterToInteractorProtocol?
    var router: SummaryPresenterToRouterProtocol?
    
    func startFetchingSummary() {
        self.interactor?.fetchSummary()
    }
    
    func showEducationController(navigationController: UINavigationController, withData education: Education) {
        self.router?.pushToEducationScreen(navigationController: navigationController, withData: education)
    }
    
    func showContestsController(navigationController: UINavigationController, withData contests: [Contest]) {
        self.router?.pushToContestsScreen(navigationController: navigationController, withData: contests)
    }
}

extension SummaryDetailPresenter: SummaryInteractorToPresenterProtocol {
    func summaryFetchedSuccess(summaryModel: Summary) {
        self.view?.show(summary: summaryModel)
    }
    
    func summaryFetchFailed() {
        self.view?.showError()
    }
}
