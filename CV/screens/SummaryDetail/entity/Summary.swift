//
//  Summary.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class Summary {
    let personalData: PersonalData?
    let personalStatement: String?
    let education: Education?
    let programmingContests: [Contest]
    
    init(_ dictionary: Dictionary<String, Any>) {
        if let personalDataDic = dictionary["personalData"] as? Dictionary<String, Any> {
            self.personalData = PersonalData(personalDataDic)
        }
        else {
            self.personalData = nil
        }
        self.personalStatement = dictionary["personalStatement"] as? String
        if let educationDic = dictionary["education"] as? Dictionary<String, Any> {
            self.education = Education(educationDic)
        }
        else {
            self.education = nil
        }
        if let programmingContestsArray = dictionary["programmingContests"] as? Array<Dictionary<String, Any>> {
            self.programmingContests = programmingContestsArray.map { Contest($0) }
        }
        else {
            self.programmingContests = []
        }
    }
}
