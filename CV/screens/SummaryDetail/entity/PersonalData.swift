//
//  personalData.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class PersonalData {
    let fullName: String?
    let photo: String?
    let address: String?
    let mobile: String?
    let email: String?
    let age: Int?
    
    init(_ dictionary: Dictionary<String, Any>) {
        self.fullName = dictionary["fullName"] as? String
        self.photo = dictionary["photo"] as? String
        self.address = dictionary["address"] as? String
        self.mobile = dictionary["mobile"] as? String
        self.email = dictionary["email"] as? String
        self.age = dictionary["age"] as? Int
    }
}
