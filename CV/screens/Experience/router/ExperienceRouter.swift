//
//  SummaryDetailRouter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class ExperienceRouter: ExperiencePresenterToRouterProtocol {
    static func createModule() -> ExperienceViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "ExperienceViewController") as! ExperienceViewController
        
        let presenter: ExperienceViewToPresenterProtocol & ExperienceInteractorToPresenterProtocol = ExperiencePresenter()
        let interactor: ExperiencePresenterToInteractorProtocol = ExperienceInteractor()
        let router:ExperiencePresenterToRouterProtocol = ExperienceRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static func createModule(withViewController view: ExperienceViewController)  {
        
        let presenter: ExperienceViewToPresenterProtocol & ExperienceInteractorToPresenterProtocol = ExperiencePresenter()
        let interactor: ExperiencePresenterToInteractorProtocol = ExperienceInteractor()
        let router:ExperiencePresenterToRouterProtocol = ExperienceRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushToCompanyScreen(navigationController: UINavigationController, withData company: Company) {
        let vc = CompanyRouter.createModule()
        vc.company = company
        navigationController.pushViewController(vc,animated: true)
    }
}
