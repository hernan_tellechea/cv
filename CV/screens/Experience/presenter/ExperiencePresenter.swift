//
//  SummaryDetailPresenter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class ExperiencePresenter: ExperienceViewToPresenterProtocol {
    
    var view: ExperiencePresenterToViewProtocol?
    var interactor: ExperiencePresenterToInteractorProtocol?
    var router: ExperiencePresenterToRouterProtocol?
    
    func startFetchingExperience() {
        self.interactor?.fetchExperience()
    }

    func showCompanyController(navigationController: UINavigationController, withData company: Company) {
        self.router?.pushToCompanyScreen(navigationController: navigationController, withData: company)
    }
}

extension ExperiencePresenter: ExperienceInteractorToPresenterProtocol {
    func experienceFetchedSuccess(companies: [Company]) {
        self.view?.show(companies: companies)
    }
    
    func experienceFetchFailed() {
        self.view?.showError()
    }
}
