//
//  ExperienceInteractor.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

class ExperienceInteractor: ExperiencePresenterToInteractorProtocol {
    
    let urlString = "https://gist.githubusercontent.com/xandarei/67d1aa2b5a8126313a345b48d1d1958d/raw/ecec6d69698e7c67f03bde319a7f28fe49572d4d/experience"
    var presenter: ExperienceInteractorToPresenterProtocol?
    
    func fetchExperience() {
        let url = URL(string: urlString)!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                self.presenter?.experienceFetchFailed()
                return
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any>, let dic = json?["content"] as? Dictionary<String, Any> else {
                self.presenter?.experienceFetchFailed()
                return
            }
            guard let array = dic["workExperience"] as? Array<Dictionary<String, Any>> else {
                self.presenter?.experienceFetchFailed()
                return
            }
            let companies = array.map { Company($0) }
            self.presenter?.experienceFetchedSuccess(companies: companies)
        }
        task.resume()
    }
}
