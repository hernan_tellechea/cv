//
//  ExperienceProtocols.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

protocol ExperienceViewToPresenterProtocol: class{
    
    var view: ExperiencePresenterToViewProtocol? {get set}
    var interactor: ExperiencePresenterToInteractorProtocol? {get set}
    var router: ExperiencePresenterToRouterProtocol? {get set}
    func startFetchingExperience()
    func showCompanyController(navigationController:UINavigationController, withData company: Company)
}

protocol ExperiencePresenterToViewProtocol: class{
    func show(companies:[Company])
    func showError()
}

protocol ExperiencePresenterToRouterProtocol: class {
    static func createModule() -> ExperienceViewController
    func pushToCompanyScreen(navigationController: UINavigationController, withData company: Company)
}

protocol ExperiencePresenterToInteractorProtocol: class {
    var presenter:ExperienceInteractorToPresenterProtocol? {get set}
    func fetchExperience()
}

protocol ExperienceInteractorToPresenterProtocol: class {
    func experienceFetchedSuccess(companies:[Company])
    func experienceFetchFailed()
}
