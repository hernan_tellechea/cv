//
//  CompanyCell.swift
//  CV
//
//  Created by Hernan Tellechea on 3/12/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {
    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
}
