//
//  ExperienceViewController.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter: ExperienceViewToPresenterProtocol?
    var companies: [Company] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        ExperienceRouter.createModule(withViewController: self)
        self.startIndicatingActivity()
        self.presenter?.startFetchingExperience()
    }
}

extension ExperienceViewController: ExperiencePresenterToViewProtocol {
    func show(companies: [Company]) {
        DispatchQueue.main.async {
            self.companies = companies
            self.stopIndicatingActivity()
            self.tableView.reloadData()
        }
    }
    
    func showError() {
        DispatchQueue.main.async {
            self.stopIndicatingActivity()
            let alert = UIAlertController(title: "Alert", message: "There was a problem fetching the experience information, please try again later.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ExperienceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath) as! CompanyCell
        let company = self.companies[indexPath.row]
        if let imageLink = company.photo, let url = URL(string: imageLink) {
            cell.companyImageView.load(url: url)
        }
        cell.companyNameLabel.text = company.name
        cell.positionLabel.text = company.position
        cell.periodLabel.text = company.period
        return cell
    }
}

extension ExperienceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let company = self.companies[indexPath.row]
        if let navigation = self.navigationController {
            self.presenter?.showCompanyController(navigationController: navigation, withData: company)
        }
    }
}
