//
//  CompanyProtocols.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

protocol CompanyViewToPresenterProtocol: class{
    
    var view: CompanyPresenterToViewProtocol? {get set}
    var interactor: CompanyPresenterToInteractorProtocol? {get set}
    var router: CompanyPresenterToRouterProtocol? {get set}
    func showProjectController(navigationController:UINavigationController, withData project: Project)
}

protocol CompanyPresenterToViewProtocol: class{

}

protocol CompanyPresenterToRouterProtocol: class {
    static func createModule() -> CompanyViewController
    func pushToProjectScreen(navigationController: UINavigationController, withData project: Project)
}

protocol CompanyPresenterToInteractorProtocol: class {
    var presenter:CompanyInteractorToPresenterProtocol? {get set}
}

protocol CompanyInteractorToPresenterProtocol: class {

}
