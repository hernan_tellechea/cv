//
//  Company.swift
//  CV
//
//  Created by Hernan Tellechea on 3/12/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class Company {
    let name: String?
    let photo: String?
    let position: String?
    let period: String?
    let projects: [Project]
    
    init(_ dictionary: Dictionary<String, Any>) {
        self.name = dictionary["company"] as? String
        self.photo = dictionary["photo"] as? String
        self.position = dictionary["position"] as? String
        self.period = dictionary["period"] as? String
        if let projectsArray = dictionary["projects"] as? Array<Dictionary<String, Any>> {
            self.projects = projectsArray.map { Project($0) }
        }
        else {
            self.projects = []
        }
    }
}
