//
//  CompanyRouter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class CompanyRouter: CompanyPresenterToRouterProtocol {
    static func createModule() -> CompanyViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "CompanyViewController") as! CompanyViewController
        
        let presenter: CompanyViewToPresenterProtocol & CompanyInteractorToPresenterProtocol = CompanyPresenter()
        let interactor: CompanyPresenterToInteractorProtocol = CompanyInteractor()
        let router:CompanyPresenterToRouterProtocol = CompanyRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushToProjectScreen(navigationController: UINavigationController, withData project: Project) {
        let vc = ProjectRouter.createModule()
        vc.project = project
        navigationController.pushViewController(vc,animated: true)
    }
}
