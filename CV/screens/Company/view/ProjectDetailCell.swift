//
//  ProjectCell.swift
//  CV
//
//  Created by Hernan Tellechea on 3/12/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class ProjectDetailCell: UITableViewCell {
    @IBOutlet weak var projectImageView: UIImageView!
    @IBOutlet weak var projectNameLabel: UILabel!
}
