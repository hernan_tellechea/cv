//
//  ExperienceViewController.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class CompanyViewController: UIViewController {

    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: CompanyViewToPresenterProtocol?
    var company: Company?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let imageLink = company?.photo, let url = URL(string: imageLink) {
            self.companyImageView.load(url: url)
        }
        self.companyNameLabel.text = self.company?.name
        self.positionLabel.text = self.company?.position
        self.periodLabel.text = self.company?.period
    }
    
}

extension CompanyViewController: CompanyPresenterToViewProtocol {

}

extension CompanyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.company?.projects.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectDetailCell", for: indexPath) as! ProjectDetailCell
        let project = self.company?.projects[indexPath.row]
        if let imageLink = project?.photo, let url = URL(string: imageLink) {
            cell.projectImageView.load(url: url)
        }
        cell.projectNameLabel.text = project?.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Projects"
    }
}

extension CompanyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let project = self.company?.projects[indexPath.row], let navigation = self.navigationController {
            self.presenter?.showProjectController(navigationController: navigation, withData: project)
        }
    }
}
