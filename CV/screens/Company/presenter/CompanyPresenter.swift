//
//  CompanyPresenter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class CompanyPresenter: CompanyViewToPresenterProtocol {
    
    var view: CompanyPresenterToViewProtocol?
    var interactor: CompanyPresenterToInteractorProtocol?
    var router: CompanyPresenterToRouterProtocol?

    func showProjectController(navigationController: UINavigationController, withData project: Project) {
        self.router?.pushToProjectScreen(navigationController: navigationController, withData: project)
    }
}

extension CompanyPresenter: CompanyInteractorToPresenterProtocol {

}
