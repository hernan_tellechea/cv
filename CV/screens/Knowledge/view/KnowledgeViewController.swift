//
//  KnowledgeViewController.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import UIKit

class KnowledgeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    let score1Image = UIImage(named:"score1")
    let score2Image = UIImage(named:"score2")
    let score3Image = UIImage(named:"score3")
    let score4Image = UIImage(named:"score4")
    let score5Image = UIImage(named:"score5")
    
    var presenter: KnowledgeViewToPresenterProtocol?
    var skills: Skills?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        KnowledgeRouter.createModule(withViewController: self)
        self.startIndicatingActivity()
        self.presenter?.startFetchingKnowledge()
    }
    
    @IBAction func didTapSegmentedControl(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
    }
}

extension KnowledgeViewController: KnowledgePresenterToViewProtocol {
    func show(skills: Skills) {
        DispatchQueue.main.async {
            self.skills = skills
            self.stopIndicatingActivity()
            self.tableView.reloadData()
        }
    }
    
    func showError() {
        DispatchQueue.main.async {
            self.stopIndicatingActivity()
            let alert = UIAlertController(title: "Alert", message: "There was a problem fetching the knowledge information, please try again later.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension KnowledgeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.segmentedControl.selectedSegmentIndex == 0 ? 1 : 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0 {
            return self.skills?.personalSkills.count ?? 0
        }
        if let techniccalSkill = self.skills?.technicalSkills {
            switch section {
            case 0: return techniccalSkill.programmingLanguages.count
            case 1: return techniccalSkill.databases.count
            case 2: return techniccalSkill.virtualMachines.count
            case 3: return techniccalSkill.certifications.count
            default: return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.segmentedControl.selectedSegmentIndex == 0 || indexPath.section != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SkillCell", for: indexPath) as! SkillPointsCell
            switch indexPath.section {
            case 0: cell.skillNameLabel.text = self.skills?.personalSkills[indexPath.row]
            case 1: cell.skillNameLabel.text = self.skills?.technicalSkills?.databases[indexPath.row]
            case 2: cell.skillNameLabel.text = self.skills?.technicalSkills?.virtualMachines[indexPath.row]
            case 3: cell.skillNameLabel.text = self.skills?.technicalSkills?.certifications[indexPath.row]
            default: cell.skillNameLabel.text = ""
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "SkillPointsCell", for: indexPath) as! SkillPointsCell
        cell.skillNameLabel.text = self.skills?.technicalSkills?.programmingLanguages[indexPath.row].language
        switch self.skills?.technicalSkills?.programmingLanguages[indexPath.row].skill ?? 0 {
        case 1:
            cell.pointsImageView.image = self.score1Image
        case 2:
            cell.pointsImageView.image = self.score2Image
        case 3:
            cell.pointsImageView.image = self.score3Image
        case 4:
            cell.pointsImageView.image = self.score4Image
        case 5:
            cell.pointsImageView.image = self.score5Image
        default:
            cell.pointsImageView.image = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.segmentedControl.selectedSegmentIndex == 0 {
            return nil
        }
        switch section {
        case 0:
            return "Programming Langages"
        case 1:
            return "Databases"
        case 2:
            return "Virtual Machines"
        case 3:
            return "Certifications"
        default:
            return nil
        }
    }
}

extension KnowledgeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}
