//
//  KnowledgePresenter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class KnowledgePresenter: KnowledgeViewToPresenterProtocol {
    
    var view: KnowledgePresenterToViewProtocol?
    var interactor: KnowledgePresenterToInteractorProtocol?
    var router: KnowledgePresenterToRouterProtocol?
    
    func startFetchingKnowledge() {
        self.interactor?.fetchKnowledge()
    }
}

extension KnowledgePresenter: KnowledgeInteractorToPresenterProtocol {
    func knowledgeFetchedSuccess(skills: Skills) {
        self.view?.show(skills: skills)
    }
    
    func knowledgeFetchFailed() {
        self.view?.showError()
    }
}
