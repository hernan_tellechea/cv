//
//  KnowledgeInteractor.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

class KnowledgeInteractor: KnowledgePresenterToInteractorProtocol {
    
    let urlString = "https://gist.githubusercontent.com/xandarei/fc36db3ed5f1d93b798a78b6d7070699/raw/d4d3ad5fee0b3ab88608e8f178abed38a416b1ed/knowledge"
    var presenter: KnowledgeInteractorToPresenterProtocol?
    
    func fetchKnowledge() {
        let url = URL(string: urlString)!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                self.presenter?.knowledgeFetchFailed()
                return
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any>, let dic = json?["content"] as? Dictionary<String, Any> else {
                self.presenter?.knowledgeFetchFailed()
                return
            }
            let skills = Skills(dic)
            self.presenter?.knowledgeFetchedSuccess(skills: skills)
        }
        task.resume()
    }
}
