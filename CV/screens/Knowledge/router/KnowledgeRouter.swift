//
//  KnowledgeRouter.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

class KnowledgeRouter: KnowledgePresenterToRouterProtocol {
    static func createModule() -> KnowledgeViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "KnowledgeViewController") as! KnowledgeViewController
        
        let presenter: KnowledgeViewToPresenterProtocol & KnowledgeInteractorToPresenterProtocol = KnowledgePresenter()
        let interactor: KnowledgePresenterToInteractorProtocol = KnowledgeInteractor()
        let router: KnowledgePresenterToRouterProtocol = KnowledgeRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static func createModule(withViewController view: KnowledgeViewController)  {
        
        let presenter: KnowledgeViewToPresenterProtocol & KnowledgeInteractorToPresenterProtocol = KnowledgePresenter()
        let interactor: KnowledgePresenterToInteractorProtocol = KnowledgeInteractor()
        let router: KnowledgePresenterToRouterProtocol = KnowledgeRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
