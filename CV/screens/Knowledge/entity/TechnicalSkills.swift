//
//  TechnicalSkills.swift
//  CV
//
//  Created by Hernan Tellechea on 3/12/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class TechnicalSkills {
    let programmingLanguages: [ProgrammingLanguage]
    let databases: [String]
    let virtualMachines: [String]
    let certifications: [String]
    
    init(_ dictionary: Dictionary<String, Any>) {
        if let programmingLanguagesArray = dictionary["programmingLanguages"] as? Array<Dictionary<String, Any>> {
            self.programmingLanguages = programmingLanguagesArray.map { ProgrammingLanguage($0) }
        }
        else {
            self.programmingLanguages = []
        }
        if let databasesArray = dictionary["databases"] as? Array<String> {
            self.databases = databasesArray
        }
        else {
            self.databases = []
        }
        if let virtualMachinesArray = dictionary["virtualMachines"] as? Array<String> {
            self.virtualMachines = virtualMachinesArray
        }
        else {
            self.virtualMachines = []
        }
        if let certificationsArray = dictionary["certifications"] as? Array<String> {
            self.certifications = certificationsArray
        }
        else {
            self.certifications = []
        }
    }
}
