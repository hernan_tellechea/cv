//
//  ProgrammingLanguage.swift
//  CV
//
//  Created by Hernan Tellechea on 3/12/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class ProgrammingLanguage {
    let language: String?
    let skill: Int?
    
    init(_ dictionary: Dictionary<String, Any>) {
        self.language = dictionary["language"] as? String
        self.skill = dictionary["skill"] as? Int
    }
}
