//
//  Skills.swift
//  CV
//
//  Created by Hernan Tellechea on 3/12/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation

public class Skills {
    let personalSkills: [String]
    let technicalSkills: TechnicalSkills?
    
    init(_ dictionary: Dictionary<String, Any>) {
        if let personalSkillsArray = dictionary["personalSkills"] as? Array<String> {
            self.personalSkills = personalSkillsArray
        }
        else {
            self.personalSkills = []
        }
        if let technicalSkillsDic = dictionary["technicalSkills"] as? Dictionary<String, Any> {
            self.technicalSkills = TechnicalSkills(technicalSkillsDic)
        }
        else {
            self.technicalSkills = nil
        }
    }
}
