//
//  KnowledgeProtocols.swift
//  CV
//
//  Created by Hernan Tellechea on 3/11/20.
//  Copyright © 2020 Hernan. All rights reserved.
//

import Foundation
import UIKit

protocol KnowledgeViewToPresenterProtocol: class{
    
    var view: KnowledgePresenterToViewProtocol? {get set}
    var interactor: KnowledgePresenterToInteractorProtocol? {get set}
    var router: KnowledgePresenterToRouterProtocol? {get set}
    func startFetchingKnowledge()
}

protocol KnowledgePresenterToViewProtocol: class{
    func show(skills: Skills)
    func showError()
}

protocol KnowledgePresenterToRouterProtocol: class {
    static func createModule() -> KnowledgeViewController
}

protocol KnowledgePresenterToInteractorProtocol: class {
    var presenter:KnowledgeInteractorToPresenterProtocol? {get set}
    func fetchKnowledge()
}

protocol KnowledgeInteractorToPresenterProtocol: class {
    func knowledgeFetchedSuccess(skills: Skills)
    func knowledgeFetchFailed()
}
